<?php 
require_once __DIR__ . "/../../helper/init.php";
if(isset($_GET['id'])) {
    $invoice_id = $_GET['id'];
    $invoice_details = $di->get('sales')->getInvoiceDetailsByInvoiceID($invoice_id);
    Util::dd($invoice_details);
}