<?php
class Sales
{
    private $table = "sales";
    private $columns = ['id', 'product_id', 'quantity', 'discount', 'invoice_id'];
    protected $di;
    private $database;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function addSales($data)
    {
        try {
            $this->database->beginTransaction();
            $data_to_be_inserted_in_invoice = [
                'customer_id' => $data['customer_id']
            ];
            $invoice_id = $this->database->insert('invoice', $data_to_be_inserted_in_invoice);
            for ($i = 0; $i < count($data['product_id']); $i++) {
                $data_to_be_inserted_in_sales = [
                    'product_id' => $data['product_id'][$i],
                    'quantity' => $data['quantity'][$i],
                    'discount' => $data['discount'][$i],
                    'invoice_id' => $invoice_id
                ];
                $this->database->insert($this->table, $data_to_be_inserted_in_sales);
            }
            Session::setSession('invoice_id', $invoice_id);
            $this->database->commit();
            return ADD_SUCCESS;
        } catch (Exception $e) {
            $this->database->rollBack();
            Util::dd($e);
            return ADD_ERROR;
        }
    }
    public function getInvoiceDetailsByInvoiceID($invoice_id)
    {
        $customer_id = $this->di->get('database')->readData('invoice', ['customer_id'], "id = {$invoice_id}")[0]->customer_id;
        $customer = $this->di->get('customer')->getCustomerByID($customer_id)[0];
        $sales = $this->di->get('database')->readData('sales', ['product_id', 'quantity', 'discount'], "invoice_id = {$invoice_id}");
        $sales_details = [];
        for ($i = 0; $i < count($sales); $i++) {
            $product = $this->di->get('product')->getProductByID($sales[$i]->product_id)[0];
            $sales_details[$i]['product'] = $product;
            $sales_details[$i]['quantity'] = $sales[$i]->quantity;
            $sales_details[$i]['discount'] = $sales[$i]->discount;
        }
        $invoice_details = [
            'invoice_id' => $invoice_id,
            'customer' => $customer,
            'sales' => $sales_details
        ];
        return $invoice_details;
    }
}
