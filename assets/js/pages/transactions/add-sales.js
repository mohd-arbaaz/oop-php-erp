var id = 2;
var baseURL = window.location.origin;
var filePath = "/helper/routing.php";

function deleteProduct(delete_id) {
  var elements = document.getElementsByClassName("product_row");
  if (elements.length != 1) {
    $("#element_" + delete_id).remove();
    updateFinalTotal();
  }
}
function addProduct() {
  $("#products_container").append(
    `<!-- PRODUCT CUSTOM CONTROL -->
        <div class="row product_row" id="element_${id}">
            <!-- CATEGORY SELECT -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Category</label>
                    <select id="category_${id}" class="form-control category_select">
                        <option disabled selected>Select Category</option>
                    </select>
                </div>
            </div>
            <!-- /CATEGORY SELECT -->
            <!-- PRODUCTS SELECT -->
            <div class="col-md-3">
                <div class="form-group">
                        <label for="">Products</label>
                        <select name="product_id[]" id="product_${id}" class="form-control product_select">
                            <option disabled selected>Select Product</option>
                        </select>
                </div>
            </div>
            <!-- /PRODUCTS SELECT -->
            <!-- SELLING PRICE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Selling Price</label>
                    <input type="number" value="0" id="selling_price_${id}" class="form-control" readonly>
                </div>
            </div>
            <!-- /SELLING PRICE -->
            <!-- QUANTITY -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity[]" id="quantity_${id}" value="0" class="form-control quantity_select">
                </div>
            </div>
            <!-- /QUANTITY -->
            <!-- DISCOUNT -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Discount</label>
                    <input type="number" name="discount[]" id="discount_${id}" class="form-control discount_select" value="0">
                </div>
            </div>
            <!-- /DISCOUNT -->
            <!-- FINAL RATE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Final Rate</label>
                    <input type="number" name="final_rate[]" id="final_rate_${id}" class="form-control" value=0 readonly>
                </div>
            </div>
            <!-- /FINAL RATE -->
            <!-- DELETE BUTTON -->
            <div class="col-md-1">
                <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top: 43%;"> 
                    <i class="far fa-trash-alt"></i>
                </button>
            </div>
            <!-- /DELETE BUTTON -->
        </div>
        <!-- /PRODUCT CUSTOM CONTROL -->`
  );
  $.ajax({
    url: baseURL + filePath,
    method: "POST",
    data: {
      getCategories: true,
    },
    dataType: "json",
    success: function (categories) {
      categories.forEach(function (category) {
        //   console.log(category);
        $("#category_" + id).append(
          `<option value='${category.id}'>${category.name}</option>`
        );
      });
      id++;
    },
  });
}

$("#products_container").on("change", ".category_select", function () {
  var element_id = $(this).attr("id").split("_")[1];
  var category_id = this.value;
  console.log('clicked');
  $.ajax({
    url: baseURL + filePath,
    method: "POST",
    data: {
      getProductsByCategoryID: true,
      categoryID: category_id,
    },
    dataType: "json",
    success: function (products) {
      $("#product_" + element_id).empty();
      $("#product_" + element_id).append(
        "<option disabled selected>Select Product</option>"
      );
      products.forEach(function (product) {
        $("#product_" + element_id).append(
          `<option value='${product.id}'>${product.name}</option>`
        );
      });
    },
  });
});

$('#products_container').on('change', '.product_select', function() {
    var element_id = $(this).attr("id").split("_")[1];
    var product_id = this.value;
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            getSellingPriceFromProductID: true,
            productID: product_id
        },
        dataType: "json",
        success: function (sellingPrice) {
            // console.log(sellingPrice[0].selling_rate);
            // console.log(element_id);
            $("#selling_price_" + element_id).val(sellingPrice[0].selling_rate);
            updateFinalRate(element_id);
        }
    });
});

$('#products_container').on('keyup click', ".quantity_select, .discount_select", function(){
    element_id = $(this).attr("id").split("_")[1];
    updateFinalRate(element_id);
});



const updateFinalRate = function(element_id){
    selling_rate = $('#selling_price_' + element_id).val();
    quantity = $('#quantity_' + element_id).val();
    discount = $('#discount_' + element_id).val();
    final_rate = (selling_rate * quantity) * ((100-discount)/100);
    // console.log(selling_rate + " " + quantity + " " + discount + " " + final_rate);
    $('#final_rate_' + element_id).val(final_rate);
    updateFinalTotal();
}

const updateFinalTotal = function(){
    product_rows = $('.product_row');
    sum = 0;
    length = product_rows.length;
    for (i = 0; i < product_rows.length; i++) {
        product_id = product_rows[i].id.split("_")[1];
        sum = sum + parseInt($('#final_rate_' + product_id).val());
    }
    $('#finalTotal').val(sum);
}

$(document).ready(function(){
    $('#add_sales_form, #add_sales_header, #add_customer_btn, .email-verify').addClass('d-none');
});

$('#check_email').on('click', function(){
    if($('#customer_email').val() === ""){
        alert('Email of Customer is blank');
    }
    else{
        email = $('#customer_email').val();
        $.ajax({
            url: baseURL + filePath,
            method: "POST",
            data: {
                getCustomerByEmail: true,
                customer_email: email
            },
            dataType: "JSON",
            success: function(customer){
                if(customer[0] == null){
                    //error customer not verified
                    $('#email_verify_success, #add_sales_header, #add_sales_form').removeClass('d-inline-block');
                    $('#email_verify_success, #add_sales_header, #add_sales_form').removeClass('d-block');
                    $('#email_verify_success, #add_sales_header, #add_sales_form').addClass('d-none');
                    $('#email_verify_fail, #add_customer_btn').addClass('d-inline-block');

                }else{
                    //customer verified show form, set customer id
                    customer_id = customer[0].id;
                    console.log(customer_id);
                    $('#email_verify_fail, #add_customer_btn').removeClass('d-inline-block');
                    $('#email_verify_success, #add_sales_form, #add_sales_header').removeClass('d-none');
                    $('#email_verify_success, #add_sales_header').addClass('d-inline-block');
                    $('#add_sales_form').addClass('d-block');
                    $('#customer_id').val(customer_id);
                }
            }
        });
    }
})

